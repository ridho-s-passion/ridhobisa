from django.urls import path
from . import views, scrape

urlpatterns = [
    path('',views.index,name ='index'),
    path('index',views.index,name ='index'),
    path('category/',views.category,name ='category'),
    path('scraping', scrape.data_fetcher,name ='scrape'),
    path('ga', views.calculate,name ='calculate'),
    path('about/',views.about,name ='about'),
    path('blog/',views.blog,name ='blog'),
]