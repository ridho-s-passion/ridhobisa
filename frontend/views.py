from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.http import HttpResponse, JsonResponse
from algorithms import ga
import json
# Create your views here.
def index(request):
	html = 'index.html'
	return render(request, html)

def about(request):
	html = 'about.html'
	return render(request, html)

def blog(request):
	html = 'blog.html'
	return render(request, html)

def category(request):
    return render(request, "category.html")

def calculate(request):
	if request.method == "POST":
		data_raw = json.loads(request.POST['data'].replace("'", "\""))
		duit = request.POST['money']
		data_best_population = ga.calculate(data_raw, duit)
		index = 0
		data = {}
		for k,v in data_raw.items():
			if data_best_population[index] == 1:
				data[k] = v
			index += 1
		print(data_best_population)
		return render(request, 'hasil.html', {'data': data})
	return HttpResponse("Halo")