from bs4 import BeautifulSoup
from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import requests
import sys
import os
import time
import json

KITABISA = "https://kitabisa.com/"
PRODUCTION = os.environ.get("PRODUCTION") != None

def create_chrome():
    chrome_options = webdriver.ChromeOptions()
    chrome_options.binary_location = os.environ.get("GOOGLE_CHROME_BIN")
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--disable-dev-shm-usage')
    if PRODUCTION:
        return webdriver.Chrome(executable_path=os.environ.get("CHROMEDRIVER_PATH"), chrome_options=chrome_options) #ENABLE IF PRODUCTION
    else:
        return webdriver.Chrome(chrome_options=chrome_options) #ENABLE IF LOCAL

def category_helper(category):
    link = "explore/"
    if category==1:
        link += "bantuan-medis"
    elif category==2:
        link += "balita-anak-sakit"
    elif category==3:
        link += "bencana-alam"
    elif category==4:
        link += "kemanusiaan"
    elif category==5:
        link += "rumah-ibadah"
    elif category==6:
        link += "kegiatan-sosial"
    elif category==7:
        link += "zakat"
    elif category==8:
        link += "beasiswa-pendidikan"
    elif category==9:
        link += "infrastruktur"
    elif category==10:
        link += "lainnya"
    elif category==11:
        link += "run-charity"
    elif category==12:
        link += "panti-asuhan"
    elif category==13:
        link += "produk-inovasi"
    elif category==14:
        link += "hadiah-apresiasi"
    elif category==15:
        link += "birthday-fundraising"
    elif category==16:
        link += "difabel"
    elif category==17:
        link += "hewan"
    elif category==18:
        link += "family-for-family"
    elif category==19:
        link += "karya-kreatif"
    elif category==20:
        link += "lingkungan"
    else:
        link = ""
    return link

"""
    parameter : Integer (category yang ingin dipilih)
    return : dict() (data kitabisa.com)
    notes : pake ini buat ambil data dari kitabisa.com
    buat dimasukkin ke GA
"""
def data_fetcher(request):
    if request.method == "POST":
        category = request.POST['category']
        driver = create_chrome()
        print("PRODUCTION :" +str(PRODUCTION))
        link = KITABISA + category_helper(int(category))
        driver.get(link)
        cnt = 40
        data_a = []
        while(cnt>0):
            driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
            cnt-=1
            data_a.extend(get_list_a(driver.page_source))
        data_a = set(data_a)
        data_a = list(data_a)
        print(data_a)
        data = scrape(data_a, driver)
        return render(request, "recommend.html", {"data": data})
    return HttpResponse("Halo")


"""
    parameter : String (take html page source sebagai arguments)
    return : dict() (data dari scraping kitabisa.com)
        
"""
def scrape(data_a, driver):
    data_real = {}
    list_of_a = data_a
    count = 0
    for x in list_of_a:
        try:
            first_div = x.findAll("div")[0]
            print(first_div)
            print("FIRST DIV")
            child_first_div = first_div.findAll("div")[1]
            print(child_first_div)
            print("CHILD FIRST DIV")
            span_child = child_first_div.findAll("span")[0]
            print(span_child)
            print("SPAN CHILD")
            data = child_first_div.findAll("div")
            print("DATA")
            print(data)
            data_child = data[4].findAll("div")
            total_kebutuhan_raw = data[3].attrs['class'][1]
            data_jumlah_terkumpul = data_child[0].findAll("span")[1].text[2:].replace(".","")
            data_jumlah_terkumpul = int(data_jumlah_terkumpul.strip())
            if data_child[1].findAll("span")[1].findAll("svg") != []:
               data_sisa_hari = sys.maxsize
            else:
               data_sisa_hari = int(data_child[1].findAll("span")[1].text.strip())
            script = js_get_styling(total_kebutuhan_raw)
            persen_terkumpul = driver.execute_script(script)
            jumlah_yang_dibutuhkan = 9999999999999 if persen_terkumpul == 0 else data_jumlah_terkumpul / persen_terkumpul

            data_real["data"+str(count)] = { \
            'link': KITABISA[:-1] + x.attrs['href'], \
            'Judul': span_child.text.replace("'","").replace("\"",""), \
            'Jumlah terkumpul': data_jumlah_terkumpul, \
            'Sisa hari': data_sisa_hari, \
            'Jumlah yang dibutuhkan' : jumlah_yang_dibutuhkan, \
            'Persentase terkumpul': persen_terkumpul }

            count += 1
        except Exception as e:
            print(e)
            continue
    driver.close()
    return data_real

"""
    return nilai persentase jumlah terkumpul dijavascript melalui selenium
"""
def js_get_styling(class_name):
    return "return (function() { \
    var p = document.createElement('div'); \
    p.className = '"+class_name+"'; \
    document.body.appendChild(p); \
    var width = (getComputedStyle(p).width); \
    var window = document.body.clientWidth; \
    return parseInt(width.replace('px','')) / window})();"

def get_list_a(html):
    soup = BeautifulSoup(html)
    return soup.findAll("a")
