# RidhoBisa
RidhoBisa adalah aplikasi yang dibuat berbasis website application dengan teknik AI menggunakan *[Beyond Classical Search](https://scele.cs.ui.ac.id/course/view.php?id=724#section-5)*.
Dalam project ini kami menggunakan Genetic Algorithm.

## Algoritma
- Input yang diberikan user adalah banyaknya donasi yang akan disumbangkan dan kategori donasi. Contoh kategori donasi dapat dilihat [disini](https://kitabisa.com/category).
- Output yang diberikan adalah rekomendasi program mana saja yang sebaiknya didanai user berdasarkan input donasi dan kategori.
- Kami memanfaatkan Genetic Algorithm (GA) untuk mendapatkan solusi pemilihan pendanaan terbaik dari beberapa program yang ada dalam suatu kategori.
- Parameter yang kami pertimbangkan untuk *Fitness Function* GA adalah sisa hari dan banyaknya dana yang diperlukan untuk suatu program. Kami merepresentasikan solusi pemilihan dengan string biner, dimana biner 1 berarti program yang bersangkutan dipilih untuk pendanaan dan biner 0 berarti tidak dipilih.

## Live Version
[Disini](https://ridhobisa.herokuapp.com)

## Cara jalankan di local
- Clone repo ini.
- Buka terminal/cmd di folder root repo ini.
- Install ChromeDriver 
- Jalankan perintah `pip install -r requirements.txt` 
- Jalankan perintah `python3 manage.py runserver 3000`

## Credits
Kelompok: **Ridho's Passion**
Anggota: 
- Muhammad Ridho Ananda - 1706028682
- Natasya Meidiana Akhda - 1706979392
- Rizkhi Pramudya Hastiputra - 1706039660
- Shafira Ishlah Nurulita - 1706979474


# RidhoBisa
