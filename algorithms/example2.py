from algorithms import ga_partial
import matplotlib.pyplot as plt

SOLUTION_PER_POPULATION = 8
ITEMS_LENGTH = 5
CAPACITY = 200
initial = [100, 200, 50, 75, 120]
target = [200, 200, 200, 200, 200]
days = [1, 7, 10, 5, 6]

best_fitness = -999999999
best_solution = []
best_outputs = []
fitness_graph = []

ga = ga_partial.GA(target, initial, days, CAPACITY)
population = ga.spawn_starting_population(SOLUTION_PER_POPULATION)
print("Initial Population")
print(population)
for solution in population:
    fitness = ga.calc_fitness(solution)
    print("fitness function: ", fitness)
    if fitness > best_fitness:
        best_fitness = fitness
        best_solution = solution

print("Initial Generation:")
print(best_solution)
best_outputs.append(best_fitness)
fitness_graph.append(best_fitness)

MAX_GENERATION = 200
for i in range(MAX_GENERATION):
    population = ga.generate_new_population(population)
    print("Generation ", i+1); print(population)
    best_current = -999999999
    for solution in population:
        fitness = ga.calc_fitness(solution)
        best_current = max(best_current, fitness)
        if fitness > best_fitness:
            best_fitness = fitness
            best_solution = solution

    best_outputs.append(best_current)
    fitness_graph.append(best_fitness)

print("best solution: ", best_solution)
print("fitness function: ", best_fitness)

print(best_outputs)
print(fitness_graph)
plt.plot(best_outputs)
plt.title("Best fitness at each iteration")
plt.show()

plt.plot(fitness_graph)
plt.title("Fitness Function Growth")
plt.show()