import random

INF = 999999999


class GA:

    def __init__(self, target, initial, days, capacity):
        self.target = target
        self.initial = initial
        self.days = days
        self.capacity = capacity
        self.items_length = len(target)

    def calc_fitness(self, solution):
        total_cost = 0
        for i in range(self.items_length):
            if solution[i] == 1:
                total_cost += self.target[i] - self.initial[i]

        if total_cost > self.capacity:
            return -INF

        fitness = 0
        for i in range(self.items_length):
            if solution[i] == 0:
                fitness -= (self.target[i] - self.initial[i]) / self.days[i]

        return fitness

    def spawn_starting_population(self, amount):
        starting_population = [self.spawn_random_individual() for i in range(amount - 1)]
        starting_population.append([random.randint(0, 0) for i in range(self.items_length)])
        return starting_population

    def spawn_random_individual(self):
        return [random.randint(0, 1) for i in range(self.items_length)]

    def generate_new_population(self, population):
        new_population = []
        mating_pool = self.select_mating_pool(population)
        pop_length = len(population)
        for i in range(pop_length):
            new_population.append(self.crossover(mating_pool[i], mating_pool[(i + 1) % pop_length]))

        for i in range(pop_length):
            new_population[i] = self.mutate(new_population[i])

        return new_population

    def select_mating_pool(self, population):
        weights = []
        fitness_sum = 0
        pop_length = len(population)
        for solution in population:
            fitness = self.calc_fitness(solution)
            fitness_sum += fitness
            weights.append(fitness)

        for i in range(pop_length):
            weights[i] = weights[i] - fitness_sum

        return random.choices(population, weights, k=pop_length)

    def crossover(self, solution1, solution2):
        items_length = len(solution1)
        crossing_point = random.randint(1, items_length - 1)
        return solution1[:crossing_point] + solution2[crossing_point:]

    def mutate(self, solution):
        chance = random.random()
        if chance <= 0.1:
            items_length = len(solution)
            index = random.randint(0, items_length - 1)
            solution[index] = 1 - solution[index]

        return solution
