import random

INF = 999999999

class GA:

    def __init__(self, target, initial, days, capacity):
        self.target = target
        self.initial = initial
        self.days = days
        self.capacity = capacity
        self.items_length = len(target)

    def calc_fitness(self, solution):
        total_cost = 0
        for i in range(self.items_length):
            total_cost += solution[i]

        if total_cost > self.capacity:
            return -INF

        fitness = 0
        for i in range(self.items_length):
            fitness -= (self.target[i] - self.initial[i] - solution[i]) / self.days[i]

        return fitness

    def spawn_starting_population(self, amount):
        starting_population = []
        for i in range(amount-1):
            remaining = self.capacity
            solution = []
            for j in range(self.items_length):
                value = random.uniform(0, min(remaining, self.target[j]-self.initial[j]))
                solution.append(value)
                remaining -= value

            starting_population.append(solution)

        starting_population.append([random.uniform(0, 0) for i in range(self.items_length)])
        return starting_population

    def generate_new_population(self, population):
        new_population = []
        mating_pool = self.select_mating_pool(population)
        pop_length = len(population)
        for i in range(pop_length):
            new_population.append(self.crossover(mating_pool[i], mating_pool[(i + 1) % pop_length]))

        for i in range(pop_length):
            new_population[i] = self.mutate(new_population[i])

        return new_population

    def select_mating_pool(self, population):
        weights = []
        fitness_sum = 0
        pop_length = len(population)
        for solution in population:
            fitness = self.calc_fitness(solution)
            fitness_sum += fitness
            weights.append(fitness)

        for i in range(len(population)):
            weights[i] = 1 - weights[i]/fitness_sum

        return random.choices(population, weights, k=pop_length)

    def crossover(self, solution1, solution2):
        crossing_point = random.randint(1, self.items_length - 1)
        return solution1[:crossing_point] + solution2[crossing_point:]

    def mutate(self, solution):
        index = random.randint(0, self.items_length - 1)
        solution[index] = solution[index] + random.uniform(-1.0, 1.0)
        return solution
