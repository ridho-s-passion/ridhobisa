## Genetic Algorithm 0-1 Knapsack
Algoritma ini memanfaatkan GA untuk menyelesaikan permasalahan 0-1 Knapsack.
Algoritma ini akan memilih program mana saja yang perlu didanai penuh sehingga fitness function optimal.
Fitness function didefinisikan sebagai berikut:<br/>
> Untuk tiap program yang tidak didanai: <br/>
> Jumlahkan negatif dari sisa dana yang belum terkumpul dibagi sisa hari