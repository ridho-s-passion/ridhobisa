from algorithms import ga_zeroone
from frontend.scrape import data_fetcher
import matplotlib.pyplot as plt

def calculate(data, duit):
	SOLUTION_PER_POPULATION = 10
	CAPACITY = int(duit)
	initial = []
	target = []
	days = []
	data_raw = data
	for k, v in data_raw.items():
		initial.append(v['Jumlah terkumpul'])
		days.append(v['Sisa hari'])
		target.append(v['Jumlah yang dibutuhkan'])

	best_fitness = -9999999999999
	best_solution = []
	best_outputs = []
	fitness_graph = []

	ga = ga_zeroone.GA(target, initial, days, CAPACITY)
	population = ga.spawn_starting_population(SOLUTION_PER_POPULATION)
	print("Initial Population")
	print(population)
	for solution in population:
	    fitness = ga.calc_fitness(solution)
	    print("fitness function: ", fitness)
	    if fitness > best_fitness:
	        best_fitness = fitness
	        best_solution = solution

	print("Initial Generation:"); print(solution)
	best_outputs.append(best_fitness)
	fitness_graph.append(best_fitness)

	MAX_GENERATION = 10000
	for i in range(MAX_GENERATION):
	    population = ga.generate_new_population(population)
	    #print("Generation ", i+1); print(population)
	    best_current = -9999999999999
	    for solution in population:
	        fitness = ga.calc_fitness(solution)
	        best_current = max(best_current, fitness)
	        if fitness > best_fitness:
	            best_fitness = fitness
	            best_solution = solution

	    best_outputs.append(best_current)
	    fitness_graph.append(best_fitness)

	print("best solution: ", best_solution)
	print("fitness function: ", best_fitness)

	
	return best_solution
